package genericClass;

import genericClass.MyData;

public class MainInvariantClass {
    public static void main(String[] args) {
        MyData<String> dataString = new MyData<>("Eko");
//        MyData<Object> dataObject = dataString; // Error

        MyData<Object> data = new MyData<>(100);
//        MyData<Integer> dataInteger = data; //Erorr

    }
}
