package genericClass;

import genericType.DataGeneric;
import genericType.Product;

public class MainGenericClass {
    public static void main(String[] args) {
        //Generic Object
        MyData<String> myDataString = new MyData<String>("Eko");
        MyData<Integer> myDataInteger = new MyData<>(100);
        var myDataBoolean = new MyData<Boolean>(true);

        System.out.println(myDataString.getData());
        System.out.println(myDataInteger.getData());
        System.out.println(myDataBoolean.getData());
    }
}