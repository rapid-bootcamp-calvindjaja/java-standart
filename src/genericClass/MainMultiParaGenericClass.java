package genericClass;

public class MainMultiParaGenericClass {
    public static void main(String[] args) {
        //Multiple Parameter Type Object
        Pair<String, Integer> pair = new Pair<String, Integer>("Eko",20);

        System.out.println(pair.getFirst());
        System.out.println(pair.getSecond());
    }
}
